// Fill out your copyright notice in the Description page of Project Settings.

#include "unrealTutorial.h"
#include "MyActor.h"


// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	box = CreateDefaultSubobject<UBoxComponent>(TEXT("ROOT"));
	box->bGenerateOverlapEvents = true;
	box->OnComponentBeginOverlap.AddDynamic(this, &AMyActor::TriggerEnter);
	box->SetRelativeScale3D(FVector(1.50f, 1.50f, 1.50f));

	RootComponent = box;

	myMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("myMesh"));
	myMesh->AttachTo(RootComponent);

	moveSpeed = 0;
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	FVector newLocation = GetActorLocation();
	float deltaHeight = (FMath::Sin(runningTime + DeltaTime) - FMath::Sin(runningTime));
	newLocation.Y += deltaHeight* moveSpeed;

	runningTime += DeltaTime;

	SetActorLocation(newLocation);

}

void  AMyActor::TriggerEnter(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult){

	// check if the other actor is a character
	if (OtherActor->IsA(ACharacter::StaticClass()))
	OtherActor->SetActorLocation(playerInitLocation);
}