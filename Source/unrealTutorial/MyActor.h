// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class UNREALTUTORIAL_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	
	UPROPERTY(EditAnyWhere)
	UShapeComponent* box;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* myMesh;

	UPROPERTY(EditAnywhere)
		float moveSpeed;

	UFUNCTION()
		void TriggerEnter(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	//UFUNCTION()
	//	void TriggerExit(class AACtor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	float runningTime;
	FVector playerInitLocation = FVector(-2041.0f, -100.0f, 235.0f);

	
	
};
