// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Actor.h"
#include "unrealTutorialCharacter.h"
#include "Item.generated.h"

UCLASS()
class UNREALTUTORIAL_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UShapeComponent* TBox;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* SM_TBox;

	AunrealTutorialCharacter* myPlayerCharacter;

	UPROPERTY(EditAnywhere)
		FString ItemName = FString(TEXT(""));

	void Pickup();
	void GetPlayer(AActor* player);

	bool bItemIsWithinRange = false;

	UFUNCTION()
		void TriggerEnter(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void TriggerExit(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex);

	
	
	
};
