// Fill out your copyright notice in the Description page of Project Settings.

#include "unrealTutorial.h"
#include "FinishLIne.h"


// Sets default values
AFinishLIne::AFinishLIne()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	finishLineBox = CreateDefaultSubobject<UBoxComponent>(TEXT("ROOT"));
	finishLineBox->bGenerateOverlapEvents = true;
	finishLineBox->OnComponentBeginOverlap.AddDynamic(this, &AFinishLIne::TriggerEnter);

	RootComponent = finishLineBox;

}

// Called when the game starts or when spawned
void AFinishLIne::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFinishLIne::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AFinishLIne::TriggerEnter(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult){


	if (OtherActor->IsA(ACharacter::StaticClass())){
	
		//OtherActor->PlaySoundOnActor(myuSound);
		OtherActor->SetActorLocation(FVector(-2041.0f, -100.0f, 235.0f));
	
	}
}