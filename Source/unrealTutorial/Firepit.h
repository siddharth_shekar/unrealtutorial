// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Actor.h"
#include "unrealTutorialCharacter.h"
#include "Firepit.generated.h"

UCLASS()
class UNREALTUTORIAL_API AFirepit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFirepit();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UShapeComponent* triggerBox;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* SM_Fire;

	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* PS_Fire;

	AunrealTutorialCharacter* playerCharacter;

	UPROPERTY(EditAnywhere)
		FString helpText = FString(TEXT("Press F to Activate the Fire"));

	int32 resetTime;

	bool bPlayerIsWihtinRange = false;
	bool bFireisLit = false;

	void GetPlayer(AActor* player);
	void Light();
	void AdvanceTimer();
	void TimerHasFinished();

	FTimerHandle CountdownTimerHandle;

	UFUNCTION()
		void TriggerEnter(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void TriggerExit(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex);
	
};
