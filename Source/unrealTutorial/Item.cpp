// Fill out your copyright notice in the Description page of Project Settings.

#include "unrealTutorial.h"
#include "Item.h"

#include "Engine.h"


// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	TBox->bGenerateOverlapEvents = true;
	TBox->OnComponentBeginOverlap.AddDynamic(this, &AItem::TriggerEnter);
	TBox->OnComponentEndOverlap.AddDynamic(this, &AItem::TriggerExit);
	RootComponent = TBox;

	SM_TBox = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BoxMesh"));
	SM_TBox->AttachTo(RootComponent);

}

void AItem::GetPlayer(AActor* player){

	myPlayerCharacter = Cast<AunrealTutorialCharacter>(player);
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();
	
}


void AItem::Pickup(){

	myPlayerCharacter->Inventory.Add(*ItemName);

	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT("item picked up"));

	Destroy();
}

// Called every frame
void AItem::Tick( float DeltaTime ){

	Super::Tick( DeltaTime );

	if (myPlayerCharacter != NULL){
	
		if (myPlayerCharacter->bIsPickUp && bItemIsWithinRange){
		
			Pickup();
		}
	}
}


void AItem::TriggerEnter(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){

	bItemIsWithinRange = true;

	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, FString::Printf(TEXT("press E to pickup %s"), *ItemName));

	GetPlayer(otherActor);

	
}


void AItem::TriggerExit(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex){

	bItemIsWithinRange = false;

}
